/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 *
 * false : All users can't access
 * true : All users can access
 * isAuthenticated : If user is log in with passport local session
 * isUser : If user have level 0
 * isAdmin : if user have level 1
 * tokenValide : if token header Authorization is valide
 *
 * By default all users must be login, have valide token and have level 0;
 */


 module.exports.policies = {

    '*': ['isAuthenticated', 'isUser', 'tokenValide'],

    'AuthController': {
      'login' :   true,
      'logout':   true,
    },

   'UsersController': {
     'create' : true,
     'getAll' : 'isAdmin',
     'destroy': 'isAdmin',
     'toAdmin': 'isAdmin',
     'toUser' : 'isAdmin'

   },

 };
