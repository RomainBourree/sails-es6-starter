/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 **/

 module.exports.routes = {

   // ********** AuthController **********
   'POST   /login'  : 'AuthController.login',
   'GET    /logout' : 'AuthController.logout',


   // ********** UsersController **********
   'GET     /users'            : 'UsersController.getAll',
   'GET     /user/:id'         : 'UsersController.getByID',
   'POST    /user'             : 'UsersController.create',
   'DELETE  /user/:id'         : 'UsersController.destroy',
   'PUT     /user/toAdmin/:id' : 'UsersController.toAdmin',
   'PUT     /user/toUser/:id' : 'UsersController.toUser'
   
 };
