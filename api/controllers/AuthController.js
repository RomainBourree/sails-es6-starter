let passport = require('passport');

// **** Log in user ****
export function login(req, res) {
  passport.authenticate('local', function(err, user, info) {
    if ((err) || (!user)) {
      return res.send({
        message: info.message,
        user: user
      });
    }
    req.logIn(user, function(err) {
      if (err) res.send(err);
      return res.send({
        message: info.message,
        user: user,
        token: jwToken.issue({id : user.id })
      });
    });

  })(req, res);
};


// **** Log out user ****
export function logout(req, res) {
  req.logout();
  res.send('logout');
};
