
// **** Create user ****
export function create(req, res){
  if(req.body.level) // Remove level access of user object and let mongoDB make default level 0;
    delete req.body.level;

  Users.create(req.body).exec(function(err, user){
    if(err)
      res.send("Error: "+err);
    else
      res.send({message: "User created !", user : user});
  });
};


// **** Delete user by ID ****
export function destroy(req, res){
  Users.destroy({id: req.param('id')}).exec(function(err){
    if(err)
      res.send("Error: "+err);
    else
      res.send({message: "User destroyed !", user: req.param('id')});
  });
};


// **** Get user by ID ****
export function getByID(req, res){
  Users.findOne({id: req.param('id')}, function(err, user){
    if(err)
      res.send("Error: "+err);
    else
      res.send({user: user});
  });
};


// **** Get all users ****
export function getAll(req, res){
  Users.find(function(err, users){
    if(err)
      res.send("Error: "+err);
    else
      res.send({users: users});
  });
};

// **** Transform User (Level 0) to Admin (Level 1) ****
export function toAdmin(req, res){
  Users.update({id : req.param('id')}, {level : 1}, function(err, users){
    if(err)
      res.send("Error: "+err);
    else
      res.send({message: "User " + req.param('id') + " is admin now !", users: users});
  });
};

// **** Transform Admin (Level 1) to User (Level 0) ****
export function toUser(req, res){
  Users.update({id : req.param('id')}, {level : 0}, function(err, users){
    if(err)
      res.send("Error: "+err);
    else
      res.send({message: "Admin " + req.param('id') + " is user now !", users: users});
  });
};
