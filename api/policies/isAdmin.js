module.exports = function(req, res, next) {
  if(req.session.passport){
    Users.findOne({id: req.session.passport.user}, function(err, user) {
      if(err || !user){
        res.forbidden("Error: "+err);
      }
      else{
        if(user.level === 1)
          return next();
        else
          return res.forbidden('You are not permitted to perform this action.');
      }
    });
  }
  else {
    return res.forbidden('You are not permitted to perform this action.');
  }
};
